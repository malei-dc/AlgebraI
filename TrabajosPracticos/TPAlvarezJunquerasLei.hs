
-- Tipos de datos a utilizar:

data BaseNucleotidica = A | C | G | T | U deriving Show

type CadenaDNA = [BaseNucleotidica]
type CadenaRNA = [BaseNucleotidica]
type Codon = (BaseNucleotidica, BaseNucleotidica, BaseNucleotidica)

data Aminoacido = Phe | Ser | Tyr | Cys | Leu | Trp | Pro | His | Arg | Gln | Ile | Thr | Asn | Lys | Met | Val | Ala | Asp | Gly | Glu deriving Show

type Proteina = [Aminoacido]

-- Empezamos con las funciones.

-- Funcion principal.
obtenerProteinas :: CadenaDNA -> [Proteina]
obtenerProteinas [] = []
obtenerProteinas cadena = sacarEspacios (obtenerProteinasAux cadena cadena cadena cadena)

complementarBase :: BaseNucleotidica -> BaseNucleotidica
complementarBase A = T
complementarBase T = A
complementarBase C = G
complementarBase G = C
complementarBase U = U

complementarCadenaDNA :: CadenaDNA -> CadenaDNA
complementarCadenaDNA [] = []
complementarCadenaDNA (x:xs) = (complementarBase x) : complementarCadenaDNA (xs)

obtenerCadenaReverseDNA :: CadenaDNA -> CadenaDNA
obtenerCadenaReverseDNA [] = []
obtenerCadenaReverseDNA (x:xs) = obtenerCadenaReverseDNA (xs) ++ [x]

transcribir :: CadenaDNA -> CadenaRNA
transcribir [] = []
transcribir (T:xs) = [U] ++ transcribir (xs)
transcribir (x:xs) = [x] ++ transcribir (xs)

traducirCodonAAminoacido:: Codon -> Aminoacido
traducirCodonAAminoacido (A, A, A) = Lys
traducirCodonAAminoacido (A, A, U) = Asn
traducirCodonAAminoacido (A, A, C) = Asn
traducirCodonAAminoacido (A, A, G) = Lys
traducirCodonAAminoacido (A, U, A) = Ile
traducirCodonAAminoacido (A, U, U) = Ile
traducirCodonAAminoacido (A, U, C) = Ile
traducirCodonAAminoacido (A, U, G) = Met
traducirCodonAAminoacido (A, C, A) = Thr
traducirCodonAAminoacido (A, C, U) = Thr
traducirCodonAAminoacido (A, C, C) = Thr
traducirCodonAAminoacido (A, C, G) = Thr
traducirCodonAAminoacido (A, G, A) = Arg
traducirCodonAAminoacido (A, G, U) = Ser
traducirCodonAAminoacido (A, G, C) = Ser
traducirCodonAAminoacido (A, G, G) = Arg
traducirCodonAAminoacido (U, A, U) = Tyr
traducirCodonAAminoacido (U, A, C) = Tyr
traducirCodonAAminoacido (U, U, A) = Leu
traducirCodonAAminoacido (U, U, U) = Phe
traducirCodonAAminoacido (U, U, C) = Phe
traducirCodonAAminoacido (U, U, G) = Leu
traducirCodonAAminoacido (U, C, A) = Ser
traducirCodonAAminoacido (U, C, U) = Ser
traducirCodonAAminoacido (U, C, C) = Ser
traducirCodonAAminoacido (U, C, G) = Ser
traducirCodonAAminoacido (U, G, U) = Cys
traducirCodonAAminoacido (U, G, C) = Cys
traducirCodonAAminoacido (U, G, G) = Trp
traducirCodonAAminoacido (C, A, A) = Gln
traducirCodonAAminoacido (C, A, U) = His
traducirCodonAAminoacido (C, A, C) = His
traducirCodonAAminoacido (C, A, G) = Gln
traducirCodonAAminoacido (C, U, A) = Leu
traducirCodonAAminoacido (C, U, U) = Leu
traducirCodonAAminoacido (C, U, C) = Leu
traducirCodonAAminoacido (C, U, G) = Leu
traducirCodonAAminoacido (C, C, A) = Pro
traducirCodonAAminoacido (C, C, U) = Pro
traducirCodonAAminoacido (C, C, C) = Pro
traducirCodonAAminoacido (C, C, G) = Pro
traducirCodonAAminoacido (C, G, A) = Arg
traducirCodonAAminoacido (C, G, U) = Arg
traducirCodonAAminoacido (C, G, C) = Arg
traducirCodonAAminoacido (C, G, G) = Arg
traducirCodonAAminoacido (G, A, A) = Glu
traducirCodonAAminoacido (G, A, U) = Asp
traducirCodonAAminoacido (G, A, C) = Asp
traducirCodonAAminoacido (G, A, G) = Glu
traducirCodonAAminoacido (G, U, A) = Val
traducirCodonAAminoacido (G, U, U) = Val
traducirCodonAAminoacido (G, U, C) = Val
traducirCodonAAminoacido (G, U, G) = Val
traducirCodonAAminoacido (G, C, A) = Ala
traducirCodonAAminoacido (G, C, U) = Ala
traducirCodonAAminoacido (G, C, C) = Ala
traducirCodonAAminoacido (G, C, G) = Ala
traducirCodonAAminoacido (G, G, A) = Gly
traducirCodonAAminoacido (G, G, U) = Gly
traducirCodonAAminoacido (G, G, C) = Gly
traducirCodonAAminoacido (G, G, G) = Gly

obtenerProteinaDeRNA :: CadenaRNA -> [Proteina]
obtenerProteinaDeRNA [] = []
obtenerProteinaDeRNA cadena =  [ juntarTodosLosAmin cadena ]

-- Devuelve true si existe el final, sino false.
sincronizaConCodonDeFin :: CadenaRNA -> Bool
sincronizaConCodonDeFin [] = False
sincronizaConCodonDeFin (U:A:A:xs) = True
sincronizaConCodonDeFin (U:A:G:xs) = True
sincronizaConCodonDeFin (U:G:A:xs) = True
sincronizaConCodonDeFin (x:y:z:xs) = sincronizaConCodonDeFin (xs)
sincronizaConCodonDeFin (_:_:xs) = False
sincronizaConCodonDeFin (_:xs) = False


-- Realiza la recursión de las 4 cadenas (una para cada proceso despues del primer principio/final).
obtenerProteinasAux :: CadenaDNA -> CadenaDNA -> CadenaDNA -> CadenaDNA -> [Proteina]
obtenerProteinasAux [] [] [] [] = []
obtenerProteinasAux cadena1 cadena2 cadena3 cadena4 = ( hacerTodoDeTodo cadena1 cadena2 cadena3 cadena4  ) ++ 
	( obtenerProteinasAux  
				(posicionAUG ( transcribir ( cadena1 ) )) 
				(obtenerCadenaReverseDNA (posicionAUG  ( transcribir ( obtenerCadenaReverseDNA ( cadena2 ) ) ))) 
				(complementarCadenaDNA (vuelvoDeUaT (posicionAUG  ( transcribir ( complementarCadenaDNA  ( vuelvoDeUaT (cadena3) ) ) ))))
				(obtenerCadenaReverseDNA (complementarCadenaDNA (vuelvoDeUaT (posicionAUG  ( transcribir ( complementarCadenaDNA (obtenerCadenaReverseDNA ( vuelvoDeUaT (cadena4) ) )) )))))  
					         )



-- Funciones Auxiliares


-- Esta funcion primero traduce y va juntando todos los aminoacidos dentro de una misma Proteina
juntarTodosLosAmin :: CadenaRNA -> Proteina
juntarTodosLosAmin [] = []
juntarTodosLosAmin (x:y:z:xs) = ( traducirCodonAAminoacido (x,y,z) ) : juntarTodosLosAmin (xs)

-- Busca la primer aparación de AUG en la cadena.
buscoAUG :: CadenaRNA -> Bool
buscoAUG [] = False
buscoAUG (A:U:G:xs) = True
buscoAUG (x:y:z:xs) = buscoAUG (y:z:xs)
buscoAUG (_:_:xs) = False
buscoAUG (_:xs) = False

-- Devuelve el "tail" de la cadena una vez encontrado el AUG y su fin.
posicionAUG :: CadenaRNA -> CadenaRNA
posicionAUG [] = []
posicionAUG (x:y:z:xs)
	| buscoAUG [x,y,z] == True && sincronizaConCodonDeFin (xs) == True = xs
	| buscoAUG (x:y:z:xs) == False = []
	| otherwise = posicionAUG (y:z:xs) 
posicionAUG (_:_:xs) = []
posicionAUG (_:xs) = []

-- Devuelve el "interior" de la cadena hasta toparse con un final (UAA, UAG , UGA)
posicionFIN :: CadenaRNA -> CadenaRNA
posicionFIN [] = []
posicionFIN (x:y:z:xs)
	| sincronizaConCodonDeFin [x,y,z] == True = []
	| otherwise = [x,y,z] ++ posicionFIN (xs)

sacarEspacios :: [Proteina] -> [Proteina]
sacarEspacios [] = []
sacarEspacios [[]] = []
sacarEspacios (x:[[]]) = [x]
sacarEspacios (x:xs) = x : sacarEspacios (xs) 

vuelvoDeUaT :: CadenaRNA -> CadenaDNA
vuelvoDeUaT [] = []
vuelvoDeUaT (U:xs) = [T] ++ vuelvoDeUaT (xs)
vuelvoDeUaT (x:xs) = [x] ++ vuelvoDeUaT (xs)


-- Realiza todas las decodificaciones de las cadenas. 
hacerTodoDeTodo :: CadenaDNA -> CadenaDNA -> CadenaDNA -> CadenaDNA -> [Proteina]
hacerTodoDeTodo [] [] [] [] = []
hacerTodoDeTodo cadena1 cadena2 cadena3 cadena4 = 
 				      obtenerProteinaDeRNA ( posicionFIN ( posicionAUG  ( transcribir ( cadena1 ) ) ) )  ++
				      obtenerProteinaDeRNA ( posicionFIN ( posicionAUG  ( transcribir ( obtenerCadenaReverseDNA ( cadena2 ) ) ) ) ) ++
				      obtenerProteinaDeRNA ( posicionFIN ( posicionAUG  ( transcribir ( complementarCadenaDNA  ( vuelvoDeUaT (cadena3) ) ) ) ) ) ++
				      obtenerProteinaDeRNA ( posicionFIN ( posicionAUG  ( transcribir ( complementarCadenaDNA  ( obtenerCadenaReverseDNA ( vuelvoDeUaT ( cadena4 ) ) ) ) ) ) )
				     


